# PageSpeed Guidlines

1. Ship less JavaScript (remove all vue related scripts at build time and add prod modules via nuxt hooks)
2. Use correctly sized images (size and weight)

- you can use this site https://compress-or-die.com/
- also, use cloudinary built in image transformations

3. Use the `loading="lazy"` attribute for `<img\>` tags
4. Use WebP when possible

- use Cloudinary built-in WebP support withthe `f_auto` flag

5. Minify html, js

- use html-minifier library (Lian, please add the correct link)

- when using `prod modules` script - make sure to build with webpack (Laravel Mix)

6. Use inline CSS in `<head\>` (default Nuxt trait)
7. Do not use resource hints excessively
8. Preload background images (esp. for mobile)
9. Use `preconnect`, `dns-prefetch` for GTM assets
10. Use lazy loading strategies for loading JS assets when possible (esp. for GTM assets)